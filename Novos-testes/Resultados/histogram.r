library(ggplot2)
power55 <- read.csv(file="file55.csv", sep=",", header=F)
power75 <- read.csv(file="file75.csv", sep=",", header=F)
power1075 <- read.csv(file="file1075.csv", sep=",", header=F)
power215 <- read.csv(file="file215.csv", sep=",", header=F)

file551 <- read.csv(file="55_1.csv", sep=",", header=F)

power[[2]]


f55 <- qplot(power55[[2]],
           geom="histogram",
           binwidth = 2,  
           main = "Potência 5.5GB",
           xlab = "Potência (W)",
           ylab = "Frequência",
           fill=I("blue"),
           col=I("red"),
           alpha=I(.5),
           xlim=c(120,160))

f75 <- qplot(power75[[2]],
             geom="histogram",
             binwidth = 2,  
             main = "Potência 7.5GB",
             xlab = "Potência (W)",
             ylab = "Frequência",
             fill=I("blue"),
             col=I("red"),
             alpha=I(.5),
             xlim=c(120,160))

f1075 <- qplot(power1075[[2]],
             geom="histogram",
             binwidth = 2,  
             main = "Potência 10.75GB",
             xlab = "Potência (W)",
             ylab = "Frequência",
             fill=I("blue"),
             col=I("red"),
             alpha=I(.5),
             xlim=c(120,160))

f215 <- qplot(power215[[2]],
             geom="histogram",
             binwidth = 2,  
             main = "Potência 21.5GB",
             xlab = "Potência (W)",
             ylab = "Frequência",
             fill=I("blue"),
             col=I("red"),
             alpha=I(.5),
             xlim=c(120,160))

f55
f75
f1075
f215

mini = 1490929280
g <- ggplot(file551,aes(x=V1,y=V2))+geom_point()+geom_line()+geom_vline(xintercept = mini)+geom_vline(xintercept =  mini+15)+geom_vline(xintercept =  mini+30)+geom_vline(xintercept =  mini+45)+geom_vline(xintercept =  mini+60);
g
