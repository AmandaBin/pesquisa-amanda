#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


int main(){

	char line[50];
	char newline[50];
	int i, j;
	int linelength;
	int epoch_beg, epoch_end;
	long int lepoch_beg, lepoch_end;
	char beg_end_str[60] = "";
	char buffer[60];
	int buffer_ts;
	long int lbuffer_ts;
	int buffer_eg;
	char bufferline[50]="";

	FILE *execpointer = fopen("epoch_timelog.txt", "rw+");
	FILE *pointer55 = fopen("energy55.txt", "rw+");
	FILE *pointer75 = fopen("energy75.txt", "rw+");
	FILE *pointer1075 = fopen("energy1075.txt", "rw+");
	FILE *pointer215 = fopen("energy215.txt", "rw+");
	FILE *merged;

	if(execpointer)
	{	
		while(fgets(line, sizeof(line), execpointer)){ 
			
			linelength = strlen(line);

			i=11;
			j=0;
			for(i=11; i<strlen(line); i++){

				if(line[i] == 'B'){
					newline[j] = line[i];
					newline[j+1] = '\n';
					newline[j+2] = '\0';
					break;
				}

				newline[j] = line[i];
				j++;
			}

			/*i=0;
			for(i=0; i<14; i++){

				if(newline[i] == '\0'){
					printf("%d", i);
				}

				printf("%c", newline[i]);
			}*/

			if( !(strcmp(newline, "Write10-75GB\n\0")) ){
				fwrite(newline, sizeof(char), strlen(newline), pointer1075);

				fgets(line, sizeof(line), execpointer);
				epoch_beg = atoi(line);
				lepoch_beg = epoch_beg;

				fgets(line, sizeof(line), execpointer);
				fgets(line, sizeof(line), execpointer);
				epoch_end = atoi(line);
				lepoch_end = epoch_end;

				snprintf(buffer, 10, "%d", epoch_beg);
				puts(buffer);

				strcat(beg_end_str, "");
				strcat(beg_end_str, buffer);
				strcat(beg_end_str, " - ");
				
				strcpy(buffer, "");
				snprintf(buffer, 10, "%ld", lepoch_end);
				strcat(beg_end_str, buffer);
				strcat(beg_end_str, "\n");


				fwrite(beg_end_str, sizeof(char), strlen(beg_end_str), pointer1075);
				strcpy(beg_end_str, "");

				merged = fopen("merged.txt", "rw+");
				while( fgets(line, sizeof(line), merged) ){

					for(i=0; i<strlen(line); i++){

						if(line[i] == '.'){
							bufferline[i] = '\0';
							break;
						}
						bufferline[i] = line[i];
					}

					buffer_ts = atoi(line);
					lbuffer_ts = buffer_ts;

					if( (lbuffer_ts >= lepoch_beg) && (lbuffer_ts <= lepoch_end) ){
	
						fwrite(line, sizeof(char), strlen(line), pointer1075);
						strcpy(buffer, "");
					}
					else
						if( (buffer_ts > epoch_end) ){
							break;
						}		
				}
				fclose(merged);


			}
			else
				if( !(strcmp(newline, "Write7-5GB\n\0")) ){
					fwrite(newline, sizeof(char), strlen(newline), pointer75);

					fgets(line, sizeof(line), execpointer);
					epoch_beg = atoi(line);
					lepoch_beg = epoch_beg;

					fgets(line, sizeof(line), execpointer);
					fgets(line, sizeof(line), execpointer);
					epoch_end = atoi(line);
					lepoch_end = epoch_end;

					snprintf(buffer, 10, "%d", epoch_beg);
					puts(buffer);

					strcat(beg_end_str, "");
					strcat(beg_end_str, buffer);
					strcat(beg_end_str, " - ");
				
					strcpy(buffer, "");
					snprintf(buffer, 10, "%ld", lepoch_end);
					strcat(beg_end_str, buffer);
					strcat(beg_end_str, "\n");


					fwrite(beg_end_str, sizeof(char), strlen(beg_end_str), pointer75);
					strcpy(beg_end_str, "");

					merged = fopen("merged.txt", "rw+");
					while( fgets(line, sizeof(line), merged) ){

						for(i=0; i<strlen(line); i++){

							if(line[i] == '.'){
								bufferline[i] = '\0';
								break;
							}
							bufferline[i] = line[i];
						}

						buffer_ts = atoi(line);
						lbuffer_ts = buffer_ts;

						if( (lbuffer_ts >= lepoch_beg) && (lbuffer_ts <= lepoch_end) ){

							fwrite(line, sizeof(char), strlen(line), pointer75);
							strcpy(buffer, "");
						}
						else
							if( (buffer_ts > epoch_end) ){
								break;
							}		
					}
					fclose(merged);


				}
				else
					if( !(strcmp(newline, "Write5-5GB\n\0")) ){
						fwrite(newline, sizeof(char), strlen(newline), pointer55);

						fgets(line, sizeof(line), execpointer);
						epoch_beg = atoi(line);
						lepoch_beg = epoch_beg;

						fgets(line, sizeof(line), execpointer);
						fgets(line, sizeof(line), execpointer);
						epoch_end = atoi(line);
						lepoch_end = epoch_end;

						snprintf(buffer, 10, "%d", epoch_beg);
						puts(buffer);

						strcat(beg_end_str, "");
						strcat(beg_end_str, buffer);
						strcat(beg_end_str, " - ");
				
						strcpy(buffer, "");
						snprintf(buffer, 10, "%ld", lepoch_end);
						strcat(beg_end_str, buffer);
						strcat(beg_end_str, "\n");


						fwrite(beg_end_str, sizeof(char), strlen(beg_end_str), pointer55);
						strcpy(beg_end_str, "");

						merged = fopen("merged.txt", "rw+");
						while( fgets(line, sizeof(line), merged) ){

							for(i=0; i<strlen(line); i++){

								if(line[i] == '.'){
									bufferline[i] = '\0';
									break;
								}
								bufferline[i] = line[i];
							}

							buffer_ts = atoi(line);
							lbuffer_ts = buffer_ts;

							if( (lbuffer_ts >= lepoch_beg) && (lbuffer_ts <= lepoch_end) ){

								fwrite(line, sizeof(char), strlen(line), pointer55);
								strcpy(buffer, "");
							}
							else
								if( (buffer_ts > epoch_end) ){
									break;
								}		
						}
						fclose(merged);


					}
					else
						if( !(strcmp(newline, "Write21-5GB\n\0")) ){
							fwrite(newline, sizeof(char), strlen(newline), pointer215);

							fgets(line, sizeof(line), execpointer);
							epoch_beg = atoi(line);
							lepoch_beg = epoch_beg;

							fgets(line, sizeof(line), execpointer);
							fgets(line, sizeof(line), execpointer);
							epoch_end = atoi(line);
							lepoch_end = epoch_end;

							snprintf(buffer, 10, "%d", epoch_beg);
							puts(buffer);

							strcat(beg_end_str, "");
							strcat(beg_end_str, buffer);
							strcat(beg_end_str, " - ");
				
							strcpy(buffer, "");
							snprintf(buffer, 10, "%ld", lepoch_end);
							strcat(beg_end_str, buffer);
							strcat(beg_end_str, "\n");


							fwrite(beg_end_str, sizeof(char), strlen(beg_end_str), pointer215);
							strcpy(beg_end_str, "");

							merged = fopen("merged.txt", "rw+");
							while( fgets(line, sizeof(line), merged) ){

								for(i=0; i<strlen(line); i++){

									if(line[i] == '.'){
										bufferline[i] = '\0';
										break;
									}
									bufferline[i] = line[i];
								}

								buffer_ts = atoi(line);
								lbuffer_ts = buffer_ts;

								if( (lbuffer_ts >= lepoch_beg) && (lbuffer_ts <= lepoch_end) ){

									fwrite(line, sizeof(char), strlen(line), pointer215);
									strcpy(buffer, "");
								}
								else
									if( (buffer_ts > epoch_end) ){
										break;
									}		
							}
							fclose(merged);

						}		

		}
	}

	return 0;
}
