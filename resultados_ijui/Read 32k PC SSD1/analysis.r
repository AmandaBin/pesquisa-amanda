library(ggplot2)

#CSVs reading
PC_SSD1_1 <- read.csv("read_PC_SSD1_32kreq_semcache-1.csv", header=T, sep=";")
PC_SSD1_2 <- read.csv("read_PC_SSD1_32kreq_semcache-2.csv", header=T, sep=";")
PC_SSD1_3 <- read.csv("read_PC_SSD1_32kreq_semcache-3.csv", header=T, sep=";")
PC_SSD1_4 <- read.csv("read_PC_SSD1_32kreq_semcache-4.csv", header=T, sep=";")
PC_SSD1_5 <- read.csv("read_PC_SSD1_32kreq_semcache-5.csv", header=T, sep=";")
PC_SSD1_6 <- read.csv("read_PC_SSD1_32kreq_semcache-6.csv", header=T, sep=";")
PC_SSD1_7 <- read.csv("read_PC_SSD1_32kreq_semcache-7.csv", header=T, sep=";")
PC_SSD1_8 <- read.csv("read_PC_SSD1_32kreq_semcache-8.csv", header=T, sep=";")
PC_SSD1_9 <- read.csv("read_PC_SSD1_32kreq_semcache-9.csv", header=T, sep=";")
PC_SSD1_10 <- read.csv("read_PC_SSD1_32kreq_semcache-10.csv", header=T, sep=";")

#putting all power measures together (with timestamp also)
power <- rbind(PC_SSD1_1, PC_SSD1_2,  PC_SSD1_3,  PC_SSD1_4,  PC_SSD1_5,  PC_SSD1_6,
                PC_SSD1_7,  PC_SSD1_8,  PC_SSD1_9,  PC_SSD1_10)

#according to another analysis, the baseline is 2.472
baseline <- 2.472

#calculation of power measures
power$voltage_pc = power$rms1*500
power$current_pc = power$rms2/5
power$power_pc = power$voltage_pc * power$current_pc
#power$current_device = (power$rms3-median(baseline$rms3))/0.66
#power$power_device = power$current_device * 5

#power measures only for test 1
PC_SSD1_1$voltage_pc = PC_SSD1_1$rms1*500
PC_SSD1_1$current_pc = PC_SSD1_1$rms2/5
PC_SSD1_1$power_pc = PC_SSD1_1$voltage_pc * PC_SSD1_1$current_pc

ggplot(data=PC_SSD1_1, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 2
PC_SSD1_2$voltage_pc = PC_SSD1_2$rms1*500
PC_SSD1_2$current_pc = PC_SSD1_2$rms2/5
PC_SSD1_2$power_pc = PC_SSD1_2$voltage_pc * PC_SSD1_2$current_pc

ggplot(data=PC_SSD1_2, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 3
PC_SSD1_3$voltage_pc = PC_SSD1_3$rms1*500
PC_SSD1_3$current_pc = PC_SSD1_3$rms2/5
PC_SSD1_3$power_pc = PC_SSD1_3$voltage_pc * PC_SSD1_3$current_pc

ggplot(data=PC_SSD1_3, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 4
PC_SSD1_4$voltage_pc = PC_SSD1_4$rms1*500
PC_SSD1_4$current_pc = PC_SSD1_4$rms2/5
PC_SSD1_4$power_pc = PC_SSD1_4$voltage_pc * PC_SSD1_4$current_pc

ggplot(data=PC_SSD1_4, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 5
PC_SSD1_5$voltage_pc = PC_SSD1_5$rms1*500
PC_SSD1_5$current_pc = PC_SSD1_5$rms2/5
PC_SSD1_5$power_pc = PC_SSD1_5$voltage_pc * PC_SSD1_5$current_pc

ggplot(data=PC_SSD1_5, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 6
PC_SSD1_6$voltage_pc = PC_SSD1_6$rms1*500
PC_SSD1_6$current_pc = PC_SSD1_6$rms2/5
PC_SSD1_6$power_pc = PC_SSD1_6$voltage_pc * PC_SSD1_6$current_pc

ggplot(data=PC_SSD1_6, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 7
PC_SSD1_7$voltage_pc = PC_SSD1_7$rms1*500
PC_SSD1_7$current_pc = PC_SSD1_7$rms2/5
PC_SSD1_7$power_pc = PC_SSD1_7$voltage_pc * PC_SSD1_7$current_pc

ggplot(data=PC_SSD1_7, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 8
PC_SSD1_8$voltage_pc = PC_SSD1_8$rms1*500
PC_SSD1_8$current_pc = PC_SSD1_8$rms2/5
PC_SSD1_8$power_pc = PC_SSD1_8$voltage_pc * PC_SSD1_8$current_pc

ggplot(data=PC_SSD1_8, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 9
PC_SSD1_9$voltage_pc = PC_SSD1_9$rms1*500
PC_SSD1_9$current_pc = PC_SSD1_9$rms2/5
PC_SSD1_9$power_pc = PC_SSD1_9$voltage_pc * PC_SSD1_9$current_pc

ggplot(data=PC_SSD1_9, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 10
PC_SSD1_10$voltage_pc = PC_SSD1_10$rms1*500
PC_SSD1_10$current_pc = PC_SSD1_10$rms2/5
PC_SSD1_10$power_pc = PC_SSD1_10$voltage_pc * PC_SSD1_10$current_pc

ggplot(data=PC_SSD1_10, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power_histogram <- qplot(power[[7]],
 #                             geom="histogram",
  #                            binwidth = 0.05,  
   #                           main = "Medidas de Potencia",
    #                          xlab = "Potência (W)",
     #                         ylab = "Frequência",
      #                        fill=I("blue"),
       #                       col=I("red"),
        #                      alpha=I(.5),
         #                     xlim=c(47.549.5))
#power_histogram

#ttimestamp <- ggplot(power,aes(x=timestamp,y=power_pc))+geom_point()+geom_line();
#ttimestamp
