library(ggplot2)

#CSVs reading
PC_SSD2_1 <- read.csv("read_PC_SSD2_32kreq_semcache-1.csv", header=T, sep=";")
PC_SSD2_2 <- read.csv("read_PC_SSD2_32kreq_semcache-2.csv", header=T, sep=";")
PC_SSD2_3 <- read.csv("read_PC_SSD2_32kreq_semcache-3.csv", header=T, sep=";")
PC_SSD2_4 <- read.csv("read_PC_SSD2_32kreq_semcache-4.csv", header=T, sep=";")
PC_SSD2_5 <- read.csv("read_PC_SSD2_32kreq_semcache-5.csv", header=T, sep=";")
PC_SSD2_6 <- read.csv("read_PC_SSD2_32kreq_semcache-6.csv", header=T, sep=";")
PC_SSD2_7 <- read.csv("read_PC_SSD2_32kreq_semcache-7.csv", header=T, sep=";")
PC_SSD2_8 <- read.csv("read_PC_SSD2_32kreq_semcache-8.csv", header=T, sep=";")
PC_SSD2_9 <- read.csv("read_PC_SSD2_32kreq_semcache-9.csv", header=T, sep=";")
PC_SSD2_10 <- read.csv("read_PC_SSD2_32kreq_semcache-10.csv", header=T, sep=";")

#putting all power measures together (with timestamp also)
power <- rbind(PC_SSD2_1, PC_SSD2_2,  PC_SSD2_3,  PC_SSD2_4,  PC_SSD2_5,  PC_SSD2_6,
               PC_SSD2_7,  PC_SSD2_8,  PC_SSD2_9,  PC_SSD2_10)

#according to another analysis, the baseline is 2.472
baseline <- 2.472

#calculation of power measures
power$voltage_pc = power$rms1*500
power$current_pc = power$rms2/5
power$power_pc = power$voltage_pc * power$current_pc
#power$current_device = (power$rms3-median(baseline$rms3))/0.66
#power$power_device = power$current_device * 5

#power measures only for test 1
PC_SSD2_1$voltage_pc = PC_SSD2_1$rms1*500
PC_SSD2_1$current_pc = PC_SSD2_1$rms2/5
PC_SSD2_1$power_pc = PC_SSD2_1$voltage_pc * PC_SSD2_1$current_pc

ggplot(data=PC_SSD2_1, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 2
PC_SSD2_2$voltage_pc = PC_SSD2_2$rms1*500
PC_SSD2_2$current_pc = PC_SSD2_2$rms2/5
PC_SSD2_2$power_pc = PC_SSD2_2$voltage_pc * PC_SSD2_2$current_pc

ggplot(data=PC_SSD2_2, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 3
PC_SSD2_3$voltage_pc = PC_SSD2_3$rms1*500
PC_SSD2_3$current_pc = PC_SSD2_3$rms2/5
PC_SSD2_3$power_pc = PC_SSD2_3$voltage_pc * PC_SSD2_3$current_pc

ggplot(data=PC_SSD2_3, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 4
PC_SSD2_4$voltage_pc = PC_SSD2_4$rms1*500
PC_SSD2_4$current_pc = PC_SSD2_4$rms2/5
PC_SSD2_4$power_pc = PC_SSD2_4$voltage_pc * PC_SSD2_4$current_pc

ggplot(data=PC_SSD2_4, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 5
PC_SSD2_5$voltage_pc = PC_SSD2_5$rms1*500
PC_SSD2_5$current_pc = PC_SSD2_5$rms2/5
PC_SSD2_5$power_pc = PC_SSD2_5$voltage_pc * PC_SSD2_5$current_pc

ggplot(data=PC_SSD2_5, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 6
PC_SSD2_6$voltage_pc = PC_SSD2_6$rms1*500
PC_SSD2_6$current_pc = PC_SSD2_6$rms2/5
PC_SSD2_6$power_pc = PC_SSD2_6$voltage_pc * PC_SSD2_6$current_pc

ggplot(data=PC_SSD2_6, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 7
PC_SSD2_7$voltage_pc = PC_SSD2_7$rms1*500
PC_SSD2_7$current_pc = PC_SSD2_7$rms2/5
PC_SSD2_7$power_pc = PC_SSD2_7$voltage_pc * PC_SSD2_7$current_pc

ggplot(data=PC_SSD2_7, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 8
PC_SSD2_8$voltage_pc = PC_SSD2_8$rms1*500
PC_SSD2_8$current_pc = PC_SSD2_8$rms2/5
PC_SSD2_8$power_pc = PC_SSD2_8$voltage_pc * PC_SSD2_8$current_pc

ggplot(data=PC_SSD2_8, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 9
PC_SSD2_9$voltage_pc = PC_SSD2_9$rms1*500
PC_SSD2_9$current_pc = PC_SSD2_9$rms2/5
PC_SSD2_9$power_pc = PC_SSD2_9$voltage_pc * PC_SSD2_9$current_pc

ggplot(data=PC_SSD2_9, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();

#power measures only for test 10
PC_SSD2_10$voltage_pc = PC_SSD2_10$rms1*500
PC_SSD2_10$current_pc = PC_SSD2_10$rms2/5
PC_SSD2_10$power_pc = PC_SSD2_10$voltage_pc * PC_SSD2_10$current_pc

ggplot(data=PC_SSD2_10, aes(x=timestamp, y=power_pc)) + geom_point() + geom_line();