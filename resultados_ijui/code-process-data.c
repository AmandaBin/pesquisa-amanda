#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//beginning of testes
#define START_HOUR 13
#define START_MINUTE 36
#define START_SECONDS 49
#define START_MS 741
#define END_HOUR 17
#define END_MINUTE 18
#define END_SECONDS 59
#define END_MS 240

//change values base_name[60], *pcssd1, *logpcssd1 and line 60 according to tests mades

long int ms_time(int hour, int minute, int seconds, int ms);


int main(){

	char file_name[200] = "";
	//change basename according to test made
	char base_name[60] = "write_PC_SSD1_4mreq_semcache-";
	char line[200];
	char buffer_number[4];
	int flag, flag_barrier;
	int test_counter; //variavel para numerar os arquivos de teste

	//beginning/end of role test
	long int starttime, endtime;
	
	//temp variables to test with the beginning/end of one test
	int tested_hour, tested_minute, tested_seconds, tested_ms;
	long int tested_mls;

	//beginning/end variables of time from one test
	int teststart_hour, teststart_minute, teststart_seconds; 
	long int teststart_ms;
	int testend_hour, testend_minute, testend_seconds;
	long int testend_ms;

	char buffer[100];
	char numberbuffer[4]="";

	//base files with data - change according to type test we want
	FILE *pcssd1 = fopen("PC_SSD1-2017-03-02-13-36-4.txt", "r");
	FILE *logpcssd1 = fopen("log_testes_PC_SSD1.txt", "r");

	//inicialization of variables of time
	starttime = ms_time(START_HOUR, START_MINUTE, START_SECONDS, START_MS);
	endtime = ms_time(END_HOUR, END_MINUTE, END_SECONDS, END_MS);

	if(logpcssd1)
	{	
		while(fgets(line, sizeof(line), logpcssd1)){

			//mudar essa linha conforme o arquivo!
			if( strstr(line,"write") && strstr(line, "semcache") && strstr(line,"4m") && !(strstr(line, "rand")) ){
				puts(line);
				test_counter++;				

				fgets(line, sizeof(line), logpcssd1);
				
				//start time string				
				fgets(line, sizeof(line), logpcssd1);

				numberbuffer[0]=line[0];
				numberbuffer[1]=line[1];
				numberbuffer[2]='\0';
				teststart_hour = atoi(numberbuffer);

				numberbuffer[0]=line[3];
				numberbuffer[1]=line[4];
				numberbuffer[2]='\0';

				teststart_minute = atoi(numberbuffer);

				numberbuffer[0]=line[6];
				numberbuffer[1]=line[7];
				numberbuffer[2]='\0';

				teststart_seconds = atoi(numberbuffer);

				printf("Hour: %d, Minute: %d, Seconds: %d\n", teststart_hour, teststart_minute, teststart_seconds);

				numberbuffer[0]='\0';


				fgets(line, sizeof(line), logpcssd1);

				//end time string
				fgets(line, sizeof(line), logpcssd1);
				numberbuffer[0]=line[0];
				numberbuffer[1]=line[1];
				numberbuffer[2]='\0';
				testend_hour = atoi(numberbuffer);

				numberbuffer[0]=line[3];
				numberbuffer[1]=line[4];
				numberbuffer[2]='\0';

				testend_minute = atoi(numberbuffer);

				numberbuffer[0]=line[6];
				numberbuffer[1]=line[7];
				numberbuffer[2]='\0';

				testend_seconds = atoi(numberbuffer);

				printf("Hour: %d, Minute: %d, Seconds: %d\n", testend_hour, testend_minute, testend_seconds);

				numberbuffer[0]='\0';


				//times in miliseconds
				teststart_ms = ms_time(teststart_hour, teststart_minute, teststart_seconds, 0);
				testend_ms = ms_time(testend_hour, testend_minute, testend_seconds, 0);

				printf("Milisseconds start: %ld\n", teststart_ms);
				printf("Milisseconds end: %ld\n", testend_ms);

				//====================================================================================

				//AQUI PROCESSAMENTO DO ARQUIVO DAS POTÊNCIAS

				//criacao do arquivo para guardar as medidas desse teste
				sprintf(buffer_number, "%d", test_counter);
				strcat(file_name, base_name);
				strcat(file_name, buffer_number);

				FILE* pointer = fopen(file_name, "w");				

				flag=1;
				flag_barrier=1;
				while(flag){

					fgets(line, sizeof(line), pcssd1);

					numberbuffer[0]=line[0];
					numberbuffer[1]=line[1];
					numberbuffer[2]='\0';
					tested_hour = atoi(numberbuffer);

					numberbuffer[0]=line[3];
					numberbuffer[1]=line[4];
					numberbuffer[2]='\0';

					tested_minute = atoi(numberbuffer);

					numberbuffer[0]=line[6];
					numberbuffer[1]=line[7];
					numberbuffer[2]='\0';

					tested_seconds = atoi(numberbuffer);

					numberbuffer[0]=line[9];
					numberbuffer[1]=line[10];
					numberbuffer[2]=line[11];
					numberbuffer[3]='\0';

					tested_ms = atoi(numberbuffer);
			
					tested_mls = ms_time(tested_hour, tested_minute, tested_seconds, tested_ms);

					//é uma medida de potencia entre 10 segundos antes do teste começar e o fim do teste
					if( (teststart_ms - tested_mls <= 10000) && (tested_mls <= testend_ms) ){
						fwrite(line, sizeof(char), strlen(line), pointer);

						//para delimitar tempo ocioso de tempo de teste
						if(flag_barrier){
							if(tested_mls >= teststart_ms){
								fwrite("-----\n", sizeof(char), strlen("-----\n"), pointer);
								flag_barrier=0;
							}
						}
					}
					else
						if(tested_mls>testend_ms){
							flag=0;
						}

				}	

				strcpy(file_name, "");			

			}
		}

	}
}


//hour in milisseconds
long int ms_time(int hour, int minute, int seconds, int ms){

	return ((hour*3600000) + (minute*60000) + (seconds*1000) + ms);

}
