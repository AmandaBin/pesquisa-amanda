import os

def quebra_linha_energia(linha):
	if ";" in linha:
		lista1 = linha.split('\n')[0].split('\r')[0].split(';')
	else: #linha separada por virgulas... pff
		lista2 = linha.split('\n')[0].split('\r')[0].split(',')
		lista1 = []
		lista1.append(lista2[0])
		lista1.append(lista2[1]+","+lista2[2])
		lista1.append(lista2[3]+","+lista2[4])
		lista1.append(lista2[5]+","+lista2[6])
	return lista1



arq = open("rms3 desligado 2017-02-22 22-15-04.csv", "r")
csvfile = "../csv/rms3_baseline.csv"
if os.path.isfile(csvfile):
	os.system("rm "+csvfile) 
csv = open(csvfile, "w")
csv.write("measurement;rms3\n")

#pula as linhas iniciais
linha = arq.readline()
while not ("RMS" in linha):
	linha = arq.readline()
#verifica a ordem
find_um = linha.find("RMS(1)")
find_dois = linha.find("RMS(2)")
find_tres = linha.find("RMS(3)")
if (find_um < find_dois) & (find_dois < find_tres):
	index_rms1 = 1
	index_rms2 = 2
	index_rms3 = 3
elif (find_tres < find_dois) & (find_dois < find_um):
	print "Ordem reversa!!!"
	index_rms1 = 3
	index_rms2 = 2
	index_rms3 = 1
else:
	print "PANIC! Eu nao entendo essa ordem!"
	print linha
	exit() 
linha = arq.readline()

#agora le o arquivo inteiro
m=0
while not ((linha == "") or (linha == "\n")):
	lista = quebra_linha_energia(linha)
	m+=1
	csv.write(str(m)+";"+str(float(lista[index_rms3].replace(",",".")))+"\n")
	
	linha = arq.readline()

arq.close()
csv.close()

