#!/bin/bash

teste1="Write5-5GB"
teste2="Write7-5GB"
teste3="Write10-75GB"
teste4="Write21-5GB"
i1=0
i2=0
i3=0
i4=0

#para funcionar com os próximos testes: 
#os arquivos gerados ficarão salvos no diretório em que o script por rodado
#lembrar de atualizar a pasta do ssd em cada teste
#verificar qual deve ser o tamanho de leitura dos arquivos na máquina que for rodar o script
#se necessário, alterar a quantidade de chamadas do iozone e o tempo ocioso entre elas

while read -rd $'\n' line; do

        if [ "$line" == "$teste1" ] 
        then
		i1=$((i1+1))		

		echo 'start time Write5-5GB-""$i1"":' >> epoch_timelog
		date +%s.%N  >> epoch_timelog

		echo "Execution Time 5-5GB p1-""$i1"":" >> exec_timelog
		/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 5767168 -I > write5-5GB-1-""$i1""
		sleep 15

		echo "Execution Time 5-5GB p2-""$i1"":" >> exec_timelog
		/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 5767168 -I > write5-5GB-2-""$i1""
		sleep 15

		echo "Execution Time 5-5GB p3-""$i1"":" >> exec_timelog
		/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 5767168 -I > write5-5GB-3-""$i1""
		sleep 15

		echo "Execution Time 5-5GB p4-""$i1"":" >> exec_timelog
		/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 5767168 -I > write5-5GB-4-""$i1""
		sleep 15

		echo 'end time Write5-5GB-""$i1"":' >> epoch_timelog
		date +%s.%N  >> epoch_timelog

		sleep 15

        else
                if [ "$line" == "$teste2" ]
                then
			i2=$((i2+1))

			echo 'start time Write7-5GB-""$i2"":' >> epoch_timelog
			date +%s.%N  >> epoch_timelog

			#roda primeira parte do teste
			echo "Execution Time 7-5GB p1-""$i2"":" >> exec_timelog
			/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 7864320 -I > write7-5GB-1-""$i2""
			sleep 20

			echo "Execution Time 7-5GB p2-""$i2"":" >> exec_timelog
			/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 7864320 -I > write7-5GB-2-""$i2""
			sleep 20

			echo "Execution Time 7-5GB p3-""$i2"":" >> exec_timelog
			/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 7864320 -I > write7-5GB-3-""$i2""
			sleep 20

			echo 'end time Write7-5GB-""$i2"":' >> epoch_timelog
			date +%s.%N  >> epoch_timelog

			sleep 15    


                else
                        if [ "$line" == "$teste3" ]
                        then 
				i3=$((i3+1))

				echo 'start time Write10-75GB-""$i3"":' >> epoch_timelog
				date +%s.%N  >> epoch_timelog

				echo "Execution Time 10-75GB p1-""$i3"":" >> exec_timelog
				/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 11272192 -I > write10-75GB-1-""$i3""
				sleep 30

				echo "Execution Time 10-75GB p2-""$i3"":" >> exec_timelog
				/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 11272192 -I > write10-75GB-2-""$i3""
				sleep 30

				echo 'end time Write10-75GB-""$i3"":' >> epoch_timelog
				date +%s.%N  >> epoch_timelog

				sleep 15


                        else 
                                if [ "$line" == "$teste4" ]
                                then
					i4=$((i4+1))

					echo 'start time Write21-5GB-""$i4"":' >> epoch_timelog
					date +%s.%N  >> epoch_timelog

					echo "Execution Time 21-5GB p1-""$i4"":" >> exec_timelog
					/usr/bin/time -f %e -o exec_timelog -a iozone -i 0 -f /mnt/ssd/temp -+n -r 1024 -s 22544384 -I > write21-5GB-1-""$i4""
					sleep 60

					echo 'end time Write21-5GB-""$i4"":' >> epoch_timelog
					date +%s.%N  >> epoch_timelog

					sleep 15


                                fi
                        fi
                fi
        fi
done < scrambled_listatestes
